from typing import List
from django.contrib import admin
from .models.tutorship import Tutorship
from .models.tutor import Tutor
from .models.guardian import Guardian
from .models.student import Student
from .models.subject import Subject
from .models.user import User

class TutorAdmin(admin.ModelAdmin):
    list_per_page = 20
    list_display = ('id', 'names', 'surnames', 'gender', 'document_type', 'doc_number', 'profession')
    search_fields = ['profession']
    list_filter = ('document_type', 'profession')

# Register your models here.

admin.site.register(Tutor, TutorAdmin)

class SubjectAdmin(admin.ModelAdmin): #Da un estilo a los datos que voy a presentar
    list_per_page = 30
    list_display = ('id', 'subject_name', 'id_tutor')
    search_fields = ['subject_name']
    list_filter = ('id', 'subject_name')

admin.site.register(Subject, SubjectAdmin)
class StudentAdmin(admin.ModelAdmin):
    list_per_page = 20
    list_display = ("id",'names','surnames','birth_date','gender','document_type',
    'doc_number','grade','phoneNumber',"id_attendat")
    search_fields = ['names']
    list_filter = ('grade','id')

admin.site.register(Student,StudentAdmin)

class GuardianAdmin(admin.ModelAdmin):
    list_per_page = 20
    list_display = ('id', 'names', 'surnames', 'gender', 'document_type', 'relationship')
    search_fields = ['relationship']
    list_filter = ('document_type', 'relatuonship')

admin.site.register(Guardian)
admin.site.register(Tutorship)
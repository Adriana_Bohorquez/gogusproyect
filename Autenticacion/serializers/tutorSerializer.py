from rest_framework import serializers
from Autenticacion.models.tutor import Tutor

class BasicTutorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tutor
        fields = ['birth_date', 'gender', 'names', 'surnames', 'profession', 'phone_number']

class TutorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tutor
        fields = ['id', 'names', 'surnames', 'birth_date', 'document_type', 'doc_number', 'gender', 'profession', 'phone_number']
from rest_framework import serializers
from Autenticacion.models.subject import Subject

class SubjectsSerializer (serializers.ModelSerializer):
    class Meta:
        model = Subject
        fields = ['id', 'subject_name', 'id_tutor']
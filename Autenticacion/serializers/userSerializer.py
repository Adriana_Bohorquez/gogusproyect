from Autenticacion.models.user import User
from rest_framework import serializers
from Autenticacion.models.student import Student
from Autenticacion.models.tutor import Tutor
from Autenticacion.models.guardian import Guardian
from Autenticacion.serializers.studentSerializer import StudentSerializer, InfoStudentSerializer
from Autenticacion.serializers.tutorSerializer import BasicTutorSerializer, TutorSerializer
from Autenticacion.serializers.guardianSerializer import FullGuardianSerializer

class UserStudentSerializer(serializers.ModelSerializer):
    account = StudentSerializer()
    guardian = FullGuardianSerializer()

    class Meta:
        model = User
        fields = ['id', 'username', 'email',
                  'password', 'typeAccount', 'account', 'guardian']

    def create(self, validated_data):
        guardian_data = validated_data.pop('guardian')
        account_data = validated_data.pop('account')
        user = User.objects.create(**validated_data)
        guardian = Guardian.objects.create(**guardian_data)
        Student.objects.create(user=user, id_attendat=guardian, **account_data)
        return user

    def to_representation(self, objUser):
        user = User.objects.get(id=objUser.id)
        account = Student.objects.get(user=objUser)
        infoUser = {
            'id': user.id,
            'username': user.username,
            'typeAccount': user.typeAccount,
            'email': user.email,
            'account': {
                'id': account.id,
                'names': account.names,
                'surnames': account.surnames,
                'document_type': account.document_type,
                'doc_number': account.doc_number,
                'grade': account.grade,
                'phoneNumber': account.phoneNumber
            }
        }
        return infoUser

class UserResumeSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['email', 'password']

class UserTutorSerializer(serializers.ModelSerializer):
    account = TutorSerializer()

    class Meta:
        model = User
        fields = ['id', 'username', 'email',
                  'password', 'typeAccount', 'account']

    def create(self, validated_data):
        account_data = validated_data.pop('account')
        user = User.objects.create(**validated_data)
        Tutor.objects.create(user=user, **account_data)
        return user

    def to_representation(self, objUser):
        user = User.objects.get(id=objUser.id)
        account = Tutor.objects.get(user=objUser)
        infoUser = {
            'id': user.id,
            'username': user.username,
            'typeAccount': user.typeAccount,
            'email': user.email,
            'account': {
                'id': account.id,
                'names': account.names,
                'surnames': account.surnames,
                'document_type': account.document_type,
                'doc_number': account.doc_number,
                'profession': account.profession,
                'phone_number': account.phone_number
            }
        }
        return infoUser

class UserTutorResumeSerializer(serializers.ModelSerializer):
    account = BasicTutorSerializer()

    class Meta:
        model = User
        fields = ['email', 'password', 'account']

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'typeAccount']
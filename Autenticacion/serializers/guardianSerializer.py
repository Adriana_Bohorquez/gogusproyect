from rest_framework import serializers
from Autenticacion.models.guardian import Guardian

class BasicGuardianSerializer(serializers.ModelSerializer):
    class Meta:
        model = Guardian
        fields = ["names", "surnames", "gender", "phone_number", "email"]

class FullGuardianSerializer(serializers.ModelSerializer):
    class Meta:
        model = Guardian
        fields = ["id", "names", "surnames", "birth_date", "gender", "document_type", "doc_number", "relationship", "phone_number", "email"]
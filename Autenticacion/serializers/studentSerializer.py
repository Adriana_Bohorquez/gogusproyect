# clase para serializador
from Autenticacion.models import guardian
from rest_framework import fields, serializers
from Autenticacion.models.student import Student, Guardian
from Autenticacion.serializers.guardianSerializer import FullGuardianSerializer


class InfoStudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ['birth_date', 'gender', 'names', 'surnames', 'grade', 'phoneNumber']


class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ['id', 'birth_date', 'names', 'surnames', 'grade',
                  'gender', 'document_type', 'doc_number', 'phoneNumber']
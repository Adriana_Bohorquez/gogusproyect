from rest_framework import serializers
from Autenticacion.models.tutorship import Tutorship

class TutorshipSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tutorship
        fields = ['id_tutorship', 'id_student', 'id_subject', 'cancelled', 
                    'feedback_tutor', 'feedback_student','score_tutor','score_student']
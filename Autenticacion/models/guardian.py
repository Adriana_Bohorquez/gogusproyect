from django.db import models

class Guardian(models.Model):

    id = models.AutoField(primary_key = True)
    names = models.CharField(max_length=40)
    surnames = models.CharField(max_length=40)
    birth_date = models.DateField()
    gender = models.CharField(max_length = 10, choices = (
        ("F", "Femenino"),
        ("M", "Masculino"),
        ("O", "Otro")
    ))
    document_type = models.CharField(max_length = 20, choices = (
        ('CC', 'CedulaCiudadania'),
        ('TI', 'TarjetaIdentidad'),
        ('P', 'Pasaporte')
    ))
    doc_number = models.CharField(max_length=60)
    relationship = models.CharField(max_length = 50)
    phone_number = models.CharField(max_length = 20)
    email = models.CharField(max_length = 40)
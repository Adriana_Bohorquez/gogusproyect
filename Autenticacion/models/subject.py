from django.db import models
from .tutor import Tutor

selec_subject = (
        ('mtm', 'matemáticas'),
        ('fisic', 'física'),
        ('estad', 'estadística'),
        ('calc', 'cálculo'),
        ('trigo', 'trigonometría'),
        ('geom', 'geometría'),
        ('finz', 'finanzas'),
        ('esp', 'español'),
        ('ingl', 'inglés'),
        ('franc', 'francés'),
        ('alem', 'alemán'),
        ('portug', 'portugués'),
        ('cisocial', 'ciencias sociales'),
        ('histo', 'historia'),
        ('geogra', 'geografía'),
        ('cinatu', 'ciencias naturales'),
        ('biolo', 'biología'),
        ('quimi', 'química'),
        ('anato', 'anatomía'),
        ('edfis', 'educación física'),
        ('music', 'música'),
        ('artes', 'artes'),
        ('dibte', 'dibujo técnico'),
        ('tecn', 'tecnología'),
        ('infrm', 'informática'),
        ('prgm', 'programación'),
        ('etic', 'ética'),
        ('relig', 'religión'),
        )

class Subject(models.Model):
    id = models.AutoField(primary_key=True)
    subject_name = models.CharField(choices=selec_subject, max_length=20)
    id_tutor = models.ForeignKey(
       Tutor,
       related_name="listado_materias",
       on_delete=models.SET_NULL,
       null=True,
       )
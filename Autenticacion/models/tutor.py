from django.db import models
from .user import User

class Tutor(models.Model):
    id = models.AutoField(primary_key=True) 
    names = models.CharField(max_length=60)
    surnames = models.CharField(max_length=60)
    birth_date = models.DateField()
    gender = models.CharField(max_length=40, choices=(
        ('F','Femenino'),
        ('M','Masculino'),
        ('O','Otro')
    ))
    document_type = models.CharField(max_length=40, choices=(
        ('CC', 'CedulaCiudadania'),
        ('TI', 'TarjetaIdentidad'),
        ('P', 'Pasaporte')
    ))
    doc_number = models.CharField(max_length=60)
    profession = models.CharField(max_length=40)
    phone_number = models.CharField(max_length=60)
    photo = models.BinaryField(null=True, blank=True)
    user = models.ForeignKey(User, related_name='user_tutor', on_delete=models.CASCADE)
#importacion de attendants
from django.db import models
from .guardian import Guardian
from .user import User

class Student(models.Model):
    id = models.AutoField(primary_key=True)
    names = models.CharField(max_length=50)
    surnames = models.CharField(max_length=50)
    birth_date = models.DateField(null=True)
    gender = models.CharField(max_length=40, choices=(
        ('F','Femenino'),
        ('M','Masculino'),
        ('O','Otro')
        ))
    document_type = models.CharField(max_length=40, choices=(
        ('RC', 'Registro Civil'),
        ('TI', 'Tarjeta Identidad'),
        ('CC', 'Cedula Ciudadania')
    ))
    doc_number = models.CharField(max_length=60,default= "ninguno")
    grade = models.IntegerField()
    phoneNumber = models.TextField(default= "ninguno")
    id_attendat = models.ForeignKey(
        Guardian, #tabla attendants 
        related_name = 'guardian_student',
        on_delete = models.CASCADE)
    user = models.ForeignKey(User, related_name='user_student', on_delete=models.CASCADE)
from django.db import models
from .student import Student
from .subject import Subject

class Tutorship(models.Model):
    id_tutorship =  models.AutoField(primary_key=True)
    id_student = models.ForeignKey(
        Student, #tabla students 
        related_name = 'student_tutorship',
        on_delete = models.CASCADE,
        null = True)
    id_subject = models.ForeignKey(
        Subject, #tabla subject 
        related_name = 'subject_tutorship',
        on_delete = models.CASCADE,
        null = True)
    cancelled = models.BooleanField()
    feedback_tutor = models.CharField(max_length=150,null=True)
    feedback_student = models.CharField(max_length=150,null=True)
    score_tutor = models.IntegerField(null=True)
    score_student = models.IntegerField(null=True)
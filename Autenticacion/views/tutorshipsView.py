from rest_framework.response import Response
from rest_framework import generics
from rest_framework import views, status
from Autenticacion.models import Tutorship
from Autenticacion.serializers import TutorshipSerializer

class TutorshipView(views.APIView):
    def get(self, request):
        queryset = Tutorship.objects.all() #Obtener objetos BD
        serialized = TutorshipSerializer(queryset, many=True) # convertir objetos de la bd
        return Response(serialized.data, status=status.HTTP_200_OK)

#List all tutorships and Create tutorships
class TutorshipListCreateView(generics.ListCreateAPIView):
    queryset=Tutorship.objects.all()
    serializer_class = TutorshipSerializer

#retrieve update delete tutorships
class TutorshipRetrieveUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Tutorship.objects.all()
    serializer_class = TutorshipSerializer    
from rest_framework import views, status
from rest_framework.response import Response
from Autenticacion.models import Guardian
from Autenticacion.serializers import FullGuardianSerializer, BasicGuardianSerializer
from rest_framework import generics
from datetime import date

class GuardianListCreate(generics.ListCreateAPIView):
    queryset = Guardian.objects.all()
    serializer_class = FullGuardianSerializer

class GuardianRetriveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Guardian.objects.all()
    serializer_class = BasicGuardianSerializer

class GuardianEstadistics(views.APIView):

    def get(self, request):

        queryset = Guardian.objects.all()
        femPercent = 0
        malPercent = 0
        othPercent = 0
        relationships = {}
        
        for guardian in queryset:
            if guardian.gender == "F":
                femPercent += 1
            elif guardian.gender == "M":
                malPercent += 1
            else:
                othPercent += 1
            if guardian.relationship not in relationships:
                relationships[guardian.relationship] = 1
            else:
                relationships[guardian.relationship] += 1
            
        femPercent /= len(queryset)
        malPercent /= len(queryset)
        othPercent /= len(queryset)
        commonRelation = max(relationships, key = lambda key: relationships[key])

        return Response(
            {
                "Porcentaje de acudientes mujeres": femPercent,
                "Porcentaje de acudientes hombres": malPercent,
                "Porcentaje de acudientes de otro genero": othPercent,
                "Relacion mas comun entre alumno y acudiente": commonRelation
            }, status.HTTP_200_OK
        )

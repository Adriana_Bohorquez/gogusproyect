from django.conf                       import settings
from rest_framework                    import generics, status
from rest_framework.response           import Response
from rest_framework.permissions        import IsAuthenticated
from rest_framework_simplejwt.backends import TokenBackend

from Autenticacion.models              import User, Student
from Autenticacion.serializers         import UserTutorResumeSerializer, UserResumeSerializer, UserSerializer, StudentSerializer, TutorSerializer

class UserUpdateView(generics.UpdateAPIView):
    """
    Clase que permite actualizar la informacion de un usuario
    """
    serializer_class = UserResumeSerializer
    permission_classes = (IsAuthenticated,)
    queryset = User.objects.all()

    def update(self, request, *args, **kwargs):
        """
        Metodo que permite actualizar la informacion de un usuario
        """
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token, verify=False)

        if valid_data['user_id'] != kwargs['pk']:
            stringResponse = {'detail':'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        
        return super().update(request, *args, **kwargs)

class UserStudentUpdateView(generics.UpdateAPIView):
    """
    Clase que permite actualizar la informacion de un usuario
    """
    serializer_class = StudentSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Student.objects.all()

    """ def update(self, request, *args, **kwargs):
        
        return super().update(request, *args, **kwargs) """

class UserStudentDeleteView(generics.DestroyAPIView):
    serializer_class   = StudentSerializer
    permission_classes = (IsAuthenticated,)
    queryset           = User.objects.all()

    def deliete(self, request, *args, **kwargs):
        token        = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data   = tokenBackend.decode(token,verify=False)
        
        if valid_data['user_id'] != kwargs['pk']:
            stringResponse = {'detail':'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        
        return super().destroy(request, *args, **kwargs)

class UserTutorUpdateView(generics.UpdateAPIView):
    """
    Clase que permite actualizar la informacion de un usuario
    """
    serializer_class = TutorSerializer
    permission_classes = (IsAuthenticated,)
    queryset = User.objects.all()

class UserTutorDeleteView(generics.DestroyAPIView):
    serializer_class   = UserTutorResumeSerializer
    permission_classes = (IsAuthenticated,)
    queryset           = User.objects.all()

    def deliete(self, request, *args, **kwargs):
        token        = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data   = tokenBackend.decode(token,verify=False)
        
        if valid_data['user_id'] != kwargs['user']:
            stringResponse = {'detail':'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        
        return super().destroy(request, *args, **kwargs)

class UserView(generics.RetrieveAPIView):
    """
    Clase que permite obtener la informacion de un usuario
    """
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)
    queryset           = User.objects.all()

    def get(self, request, *args, **kwargs):
        """
        Metodo que permite obtener la informacion de un usuario
        """
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token, verify=False)

        if valid_data['user_id'] != kwargs['pk']:
            stringResponse = {'detail':'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        
        return super().get(request, *args, **kwargs)
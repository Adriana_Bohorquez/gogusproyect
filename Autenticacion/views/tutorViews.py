from rest_framework import views, status
from rest_framework.response import Response
from Autenticacion.models import Tutor
from Autenticacion.serializers import TutorSerializer
from rest_framework import generics


class TutorView(views.APIView):

    def get(self, request):
        queryset = Tutor.objects.all()  # Obtener objetos base de datos
        # Convertir objetos de la base de datos a JSON
        serialized = TutorSerializer(queryset, many=True)
        numMen = 0
        numWomen = 0
        for tutor in queryset:
            if tutor.gender == "F":
                numWomen += 1
            elif tutor.gender == "M":
                numMen += 1
        return Response({"number of woman tutors": numWomen, "number of man tutors": numMen}, status=status.HTTP_200_OK)


class TutorListCreateView(generics.ListCreateAPIView):
    queryset = Tutor.objects.all()
    serializer_class = TutorSerializer

# Retrieve, Update, Delete a account


class TutorRetrieveUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Tutor.objects.all()
    serializer_class = TutorSerializer

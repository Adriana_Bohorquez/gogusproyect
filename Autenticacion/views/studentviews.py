from rest_framework import views, status
from rest_framework.response import Response
from rest_framework.serializers import Serializer
from Autenticacion.models import Student
from Autenticacion.serializers.studentSerializer import InfoStudentSerializer,StudentSerializer
from rest_framework import generics

#listar informacion de los estudiantes (get)
class InfoStudentview(views.APIView):
    def get(self,request):
        queryset = Student.objects.all()
        #serializador convierte un objeto de la base de datos a un diccionario
        serialized = InfoStudentSerializer(queryset,many=True)
        return Response(serialized.data,status=status.HTTP_200_OK)

#listar todos los estudiantes y crear un estudiantes
class StudentListCreateview(generics.ListCreateAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer

#Retreve, Update, Delete Strudent
class StudentRetrieveUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer

        
        
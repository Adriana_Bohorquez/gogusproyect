from rest_framework.response import Response
from rest_framework import generics
from rest_framework import views, status
from Autenticacion.models import Subject
from Autenticacion.serializers import SubjectsSerializer

class SubjectView(views.APIView):
    def get(self, request):
        queryset = Subject.objects.all() #Obtener objetos BD
        serialized = SubjectsSerializer(queryset, many=True) # convertir objetos de la bd
        return Response(serialized.data, status=status.HTTP_200_OK)

# List all subjects and Create one
class SubjectListCreateView(generics.ListCreateAPIView):
        queryset = Subject.objects.all() 
        serializer_class = SubjectsSerializer

#Retreve update, delete a subject
class SubjectRetrieveUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
        queryset = Subject.objects.all()
        serializer_class = SubjectsSerializer
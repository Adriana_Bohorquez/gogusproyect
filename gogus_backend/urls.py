from django.contrib import admin
from django.urls import path, include
from Autenticacion.views.studentviews import InfoStudentview, StudentListCreateview, StudentRetrieveUpdateDeleteView
from Autenticacion.views import TutorView, TutorListCreateView, TutorRetrieveUpdateDeleteView
from Autenticacion.views.subjectsViews import SubjectView, SubjectListCreateView, SubjectRetrieveUpdateDeleteView
from Autenticacion.views.tutorshipsView import TutorshipListCreateView, TutorshipRetrieveUpdateDeleteView
from Autenticacion.views.guardianView import GuardianListCreate, GuardianRetriveUpdateDestroy, GuardianEstadistics
from Autenticacion.views import UserStudentDeleteView, UserTutorDeleteView, UserStudentUpdateView, UserTutorUpdateView, UserView, UserUpdateView
from Autenticacion import views
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('rest-auth/', include('rest_auth.urls')),  
    path('tutors/', TutorListCreateView.as_view()),
    path('tutors/numbers', TutorView.as_view()),
    path('tutor/<int:pk>', TutorRetrieveUpdateDeleteView.as_view()),
    path('infostudents/',InfoStudentview.as_view()),
    path('students/',StudentListCreateview.as_view()),
    path('student/<int:pk>/',StudentRetrieveUpdateDeleteView.as_view()),
    path('tutorship/', TutorshipListCreateView.as_view()),
    path('tutorship/<int:pk>', TutorshipRetrieveUpdateDeleteView.as_view()),
    path('subjects/', SubjectView.as_view()),
    path('subjectscv/', SubjectListCreateView.as_view()),
    path('subjectsrud/<int:pk>', SubjectRetrieveUpdateDeleteView.as_view()),
    path('acudientes/', GuardianListCreate.as_view()),
    path('acudiente/<int:pk>/', GuardianRetriveUpdateDestroy.as_view()),
    path('estadisticasacudientes/', GuardianEstadistics.as_view()),
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.UserCreateView.as_view()),
    path('user/<int:pk>/', views.UserView.as_view()),
    path('userStudent/<int:pk>/', views.UserStudentDetailView.as_view()),
    path('userStudent/update/<int:pk>/', views.UserStudentUpdateView.as_view()),
    path('userStudent/delete/<int:pk>/', views.UserStudentDeleteView.as_view()),
    path('userTutor/<int:pk>/', views.UserTutorDetailView.as_view()),
    path('userTutor/update/<int:pk>/', views.UserTutorUpdateView.as_view()),
    path('userTutor/delete/<int:pk>/', views.UserTutorDeleteView.as_view()),
    path('user/update/<int:pk>/', views.UserUpdateView.as_view()),
]   


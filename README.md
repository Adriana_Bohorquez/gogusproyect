README

Proyecto de la semana 1, P52, Ciclo 3, Mision TIC 2022
Equipo 2

    Stephanie Avila
    Anna Gabriela Salazar
    William I. Jimenez
	Haider Mayorga
    Josue Nuñez
	Adriana Bohorquez

Proyecto en Jira
Descripcion del programa

Este programa es una aplicación web (backend) realizada con Django y base de datos en Heroku con el fin de ayudar a conectar de manera voluntaria a potenciales tutores con estudios técnicos, tecnológicos, en últimos semestres (8vo en adelante), y/o profesionales, con estudiantes en etapa escolar (primaria y bachillerato) para brindar tutoría en temas con los que requieran refuerzo o apoyo para realizar actividades académicas a través de sesiones sin ánimo de lucro agendadas en la aplicación, la cual al finalizar, permitirá que el tutor genere una calificación y una corta realimentación escrita al estudiante en cuanto a falencias y fortalezas para un mejor seguimiento, de igual manera el estudiante tendrá la opción de dar una calificación numérica y un comentario frente a la tutoría recibida.

    Las importaciones necesarias para que el programa corra estan en el documento requeriments.txt